﻿using NSubstitute;
using NUnit.Framework;
using Roulette;

namespace RouletteTests
{
    [TestFixture]
    public class GameTests
    {
        private static Bet DefaultBet = new Bet { Amount = 6.3f, Number = 2 };

        [Test]
        public void Run_SeveralRounds_WheelSpinsExpectedNumberOfTimes()
        {
            // Setup
            const int numberOfRounds = 5;
            var wheel = Substitute.For<IWheel>();
            var game = CreateGame(wheel);

            // Exercise
            game.Run(numberOfRounds);

            // Verify
            wheel.Received(numberOfRounds).Spin();
        }

        [TestCase(1)]
        [TestCase(4)]
        public void Run_PlayerAlwaysWins_PlayerReceivesExpectedGainEveryTime(int numberOfRounds)
        {
            var playersBet = new Bet { Amount = 1.5f, Number = 12 };
            var wheel = CreateWheel(output : playersBet.Number);
            var player = CreatePlayer(playersBet);
            var game = new Game(wheel, player);

            game.Run(numberOfRounds);

            float expectedPayout = playersBet.Amount * Game.PayoutMultiplier + playersBet.Amount;
            player.Received(numberOfRounds).ReceiveMoney(expectedPayout);
        }

        [TestCase(1)]
        [TestCase(3)]
        public void Run_PlayerAlwaysLoses_PlayerDoesNotGetPaid(int numberOfRounds)
        {
            var playersBet = CreateBet(number: 11);
            var wheel = CreateWheel(output: 12);
            var player = CreatePlayer(playersBet);
            var game = new Game(wheel, player);

            game.Run(numberOfRounds);

            player.DidNotReceiveWithAnyArgs().ReceiveMoney(0f);
        }



        private static Game CreateGame(IWheel wheel)
        {
            var player = CreatePlayer();
            var game = new Game(wheel, player);
            return game;
        }

        private static IPlayer CreatePlayer()
        {
            return CreatePlayer(DefaultBet);
        }

        private static IPlayer CreatePlayer(Bet bet)
        {
            var player = Substitute.For<IPlayer>();
            player.GetBet().Returns(bet);
            return player;
        }

        private static IWheel CreateWheel(int output)
        {
            var wheel = Substitute.For<IWheel>();
            wheel.Spin().Returns(output);
            return wheel;
        }

        private Bet CreateBet(int number)
        {
            return new Bet { Amount = 1f, Number = number };
        }
    }
}