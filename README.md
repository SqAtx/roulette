# Roulette
I wrote this little project to support my presentation at the Junior Developer Workshop at Global Relay on April 7th, 2017.

You can find the [slides](https://docs.google.com/presentation/d/1iu-rnybaaV2pPEZwXCSvnlJkBOxK_NUuyCoUvUostIA/edit?usp=sharing) I used for the presentation, as well as my [notes](https://docs.google.com/document/d/1HFOq7P25yTlvEjzZ9QE2ejSOtwFNDVsuIrOomv6SuRI/edit?usp=sharing), on Google Docs.