﻿using System;

namespace Roulette
{
    public interface IWheel
    {
        int Spin();
    }

    public class Wheel : IWheel
    {
        private readonly Random _rand = new Random();

        public int Spin()
        {
            return _rand.Next(0, 36);
        }
    }
}