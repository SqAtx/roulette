﻿namespace Roulette
{
    public interface IPlayer
    {
        Bet GetBet();
        void ReceiveMoney(float amount);
    }

    public class Player : IPlayer
    {
        private readonly string _name;

        public Player(string name)
        {
            _name = name;
        }

        public Bet GetBet()
        {
            throw new System.NotImplementedException();
        }

        public void ReceiveMoney(float amount)
        {
            throw new System.NotImplementedException();
        }
    }
}
