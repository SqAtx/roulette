﻿using System;

namespace Roulette
{
    public class Game
    {
        public const int PayoutMultiplier = 35;

        private readonly IWheel _wheel;
        private readonly IPlayer _player;

        public Game(IWheel wheel, IPlayer player)
        {
            _wheel = wheel;
            _player = player;
        }

        public void Run(int numberOfRounds)
        {
            Bet playersBet = _player.GetBet();

            for (var i = 0; i < numberOfRounds; i++)
            {
                int result = _wheel.Spin();

                if (result == playersBet.Number)
                {
                    float payout = playersBet.Amount * PayoutMultiplier + playersBet.Amount;
                    _player.ReceiveMoney(payout);
                }
            }
        }
    }
}
