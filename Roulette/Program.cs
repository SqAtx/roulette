﻿namespace Roulette
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var wheel = new Wheel();
            var player = new Player("John Doe");
            var game = new Game(wheel, player);

            game.Run(1);
        }
    }
}
